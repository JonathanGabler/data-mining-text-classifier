## Initialisation
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import copy

np.random.seed(100)
k = 3

# get random numbers for x and y
dataFrame = pd.DataFrame({
    'x': np.random.randint(1, 99, size=20),
    'y': np.random.randint(1, 99, size=20)
})
# centroids[i] = [x, y]
centroids = {
    i + 1: [np.random.randint(0, 100), np.random.randint(0, 100)]
    for i in range(k)
}

# Plot the initial cluster with only the centroids highlighted
fig = plt.figure(figsize=(5, 5))
plt.scatter(dataFrame['x'], dataFrame['y'], color='k')
plotmap = {1: 'm', 2: 'y', 3: 'b', 4: 'c'}
for i in centroids.keys():
    plt.scatter(*centroids[i], color=plotmap[i])
plt.xlim(0, 100)
plt.ylim(0, 100)
plt.title('Initial Plot Euclidean')

plt.show()


# Assignment Stage
# Here we will assign the plots in the neighborhood to their cloest centroid
def assignment(df, centroids):
    for i in centroids.keys():
        # sqrt((x1 - x2)^2 - (y1 - y2)^2) Euclidean
        df['distance_from_{}'.format(i)] = (
            np.sqrt(
                (df['x'] - centroids[i][0]) ** 2 + (df['y'] - centroids[i][1]) ** 2
            )
        )
        # Plot the table with the values
    centroid_distance_cols = ['distance_from_{}'.format(i) for i in centroids.keys()]
    df['closest'] = df.loc[:, centroid_distance_cols].idxmin(axis=1)
    df['closest'] = df['closest'].map(lambda x: int(x.lstrip('distance_from_')))
    df['color'] = df['closest'].map(lambda x: plotmap[x])
    return df

dataFrame = assignment(dataFrame, centroids)

fig = plt.figure(figsize=(5, 5))
plt.scatter(dataFrame['x'], dataFrame['y'], color=dataFrame['color'], alpha=0.5, edgecolor='k')
for i in centroids.keys():
    plt.scatter(*centroids[i], color=plotmap[i])
plt.xlim(0, 100)
plt.ylim(0, 100)
plt.title('Assign Points Euclidean')
plt.show()

# Update Stage
old_centroids = copy.deepcopy(centroids)


def update(centroids):
    for i in centroids.keys():
        centroids[i][0] = np.mean(dataFrame[dataFrame['closest'] == i]['x'])
        centroids[i][1] = np.mean(dataFrame[dataFrame['closest'] == i]['y'])
    return centroids


centroids = update(centroids)

fig = plt.figure(figsize=(5, 5))
ax = plt.axes()
plt.scatter(dataFrame['x'], dataFrame['y'], color=dataFrame['color'], alpha=0.5, edgecolor='k')
for i in centroids.keys():
    plt.scatter(*centroids[i], color=plotmap[i])
plt.xlim(0, 100)
plt.ylim(0, 100)
for i in old_centroids.keys():
    old_x = old_centroids[i][0]
    old_y = old_centroids[i][1]
    dx = (centroids[i][0] - old_centroids[i][0]) * 0.75
    dy = (centroids[i][1] - old_centroids[i][1]) * 0.75
plt.title('Reassign Points Euclidean')
plt.show()

# Repeat Assigment Stage

dataFrame = assignment(dataFrame, centroids)

# Plot results
fig = plt.figure(figsize=(5, 5))
plt.scatter(dataFrame['x'], dataFrame['y'], color=dataFrame['color'], alpha=0.5, edgecolor='k')
for i in centroids.keys():
    plt.scatter(*centroids[i], color=plotmap[i])
plt.xlim(0, 100)
plt.ylim(0, 100)
plt.title('Reassign Points Euclidean')
plt.show()

# Repeat till we have final clusters
while True:
    closest_centroids = dataFrame['closest'].copy(deep=True)
    centroids = update(centroids)
    dataFrame = assignment(dataFrame, centroids)
    if closest_centroids.equals(dataFrame['closest']):
        break

fig = plt.figure(figsize=(5, 5))
plt.scatter(dataFrame['x'], dataFrame['y'], color=dataFrame['color'], alpha=0.5, edgecolor='k')
for i in centroids.keys():
    plt.scatter(*centroids[i], color=plotmap[i])
plt.xlim(0, 100)
plt.ylim(0, 100)
plt.title('Final Points Euclidean')
plt.show()
print(dataFrame.head())